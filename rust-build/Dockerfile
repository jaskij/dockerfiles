# syntax=docker/dockerfile:1.4

ARG RUST_VERSION=1.82

FROM rust:${RUST_VERSION}-bookworm

ENV CARGO_REGISTRIES_CRATES_IO_PROTOCOL=sparse

RUN apt-get update && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        gcc-aarch64-linux-gnu \
        g++-aarch64-linux-gnu \
        binutils-aarch64-linux-gnu \
        libc6-dev-arm64-cross \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

RUN rustup target install \
        aarch64-unknown-linux-gnu \
        x86_64-unknown-linux-gnu \
    && rustup toolchain install nightly \
    && rustup component add clippy

RUN cargo +nightly install --locked cargo-udeps

RUN cargo install --locked \
        cargo-deny \
        cargo-hack \
        cargo-nextest \
        cargo-llvm-cov \
    && cargo +nightly install --locked cargo-udeps \
    && rm -rf $CARGO_HOME/git $CARGO_HOME/registry \
    && strip $CARGO_HOME/bin/*

ENV \
    CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER=aarch64-linux-gnu-gcc \
    CC_aarch64_unknown_linux_gnu=aarch64-linux-gnu-gcc \
    CXX_aarch64_unknown_linux_gnu=aarch64-linux-gnu-g++
